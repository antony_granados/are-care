var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

gulp.task('lint', require('./gulp-tasks/lint_task')(gulp, plugins));
gulp.task('sass', require('./gulp-tasks/sass_build')(gulp, plugins));

gulp.task('default', ['lint', 'sass'], function () {
    gulp.watch('assets/js/**/*.js', ['scripts']);
    gulp.watch('assets/sass/**/*.scss', ['sass']);
});
