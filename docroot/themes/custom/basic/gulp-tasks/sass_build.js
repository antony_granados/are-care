module.exports = function (gulp, plugins) {
  return function () {
      gulp.src('./assets/sass/*.scss')
          .pipe(
            plugins.sass(
              {
                outputStyle: 'expanded',
                precision: 10,
                includePaths: ['.']
              }
            )
          )
          .on('error', plugins.sass.logError)
          .pipe(
            plugins.autoprefixer(
              {
                browsers: ['last 2 versions', 'last 8 iOS versions', 'ie >= 10'],
                cascade: false
              }
            )
          )
          .pipe(gulp.dest('./assets/css/'));
  };
};