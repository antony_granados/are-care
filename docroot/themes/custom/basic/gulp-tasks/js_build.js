import gulp     from 'gulp';
import include  from 'gulp-include';
import rename   from 'gulp-rename';
import babel    from 'gulp-babel';

let build_factory = (src, destName, destFolder)=>{
  return () => {
    return gulp
      .src(src)
      .pipe(include())
        .on('error', console.log)
      .pipe(rename(destName))
      .pipe(babel())
      .pipe(gulp.dest(destFolder));
  } 
};

const util_build = build_factory('./assets/js/util/index.js', 'util.js', "assets/js/" );
const components_build = build_factory('./assets/js/components/index.js', 'components.js', "assets/js/" );

const vendor_build = () => {
  return gulp
      .src('./assets/js/vendor/index.js')
      .pipe(include())
        .on('error', console.log)
      .pipe(rename('vendor.js'))
      .pipe(gulp.dest("assets/js/"));
};

export {util_build, vendor_build, components_build}